python-clapp JAT test suite
===========================

This is an independent test suite for python-clapp that makes use
of JATS - Just A Test System.

JATS defines test suite format, which is a directory tree consisting of
individual tests.  These individual tests can in principle be implemented
in any language, but for now *shellfu-bash-jat* is the only available
library.
